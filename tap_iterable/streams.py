"""Stream type classes for tap-iterable."""
import json
from sqlite3 import Cursor
from typing import Any, cast, Iterable
from backports.cached_property import cached_property

import requests
from singer_sdk import typing as th
import copy
from singer_sdk.exceptions import RetriableAPIError
from singer_sdk.helpers.jsonpath import extract_jsonpath

from tap_iterable.client import IterableStream


class ListsStream(IterableStream):
    """Lists stream."""

    name = "lists"
    path = "/lists"
    records_jsonpath = "$.lists[*]"
    primary_keys = ["id"]
    replication_key = None

    schema = th.PropertiesList(
        th.Property("id", th.IntegerType),
        th.Property("name", th.StringType),
        th.Property("createdAt", th.IntegerType),
        th.Property("listType", th.StringType),
    ).to_dict()

    def get_child_context(self, record: dict, context) -> dict:
        if record["listType"] == "Dynamic":
            return {"list_id": record["id"]}
        return {"list_id": None}


class ListsSelectStream(IterableStream):
    """List Select stream."""
    name = "list_select"
    path = "/lists"
    records_jsonpath = "$.lists[*]"
    primary_keys = ["email"]
    replication_key = None

    def request_iterable(self, url, endpoint, params=None):
        headers = {"Api-Key":self.config.get("api_key")}
        url = f"{url}{endpoint}"
        if params is not None:
            url = f"{url}?{params}"
        response = requests.get(url,headers=headers)
        if response.status_code>=400:
            raise RetriableAPIError
        return response

    @cached_property
    def schema(self):
        decorated_request = self.request_decorator(self.request_iterable)
        lists = decorated_request(self.url_base, "/lists").json()
        lists = lists["lists"]
        properties = [th.Property(str(list["id"]), th.IntegerType) for list in lists]
        return th.PropertiesList(*properties).to_dict()
    
    def parse_response(self, response: requests.Response) -> Iterable[dict]:
        lists = extract_jsonpath(self.records_jsonpath, input=response.json())
        return [{l["name"]: l["id"] for l in lists}]
    
    def get_child_context(self, record, context):
        lists = [int(v) for v in self.selected_properties]
        return {"selected_lists": lists}


class UsersStream(IterableStream):
    """Lists stream."""

    name = "users"
    path = "/export/data.json?dataTypeName=user&range=All"
    primary_keys = ["email"]
    replication_key = None

    def parse_response(self, response: requests.Response) -> Iterable[dict]:
        """Parse the response and return an iterator of result rows."""
        payload = response.text
        users = payload.rstrip("\n").split("\n")
        for user in users:
            yield json.loads(user)

    @property
    def schema(self) -> dict:
        """Dynamically detect the json schema for the stream.
        This is evaluated prior to any records being retrieved.
        """
        # Return cached schema, if it exists
        if "schema" in self._tap.cache:
            return self._tap.cache["schema"]

        # Init request session
        self._requests_session = requests.Session()
        # Get the data from Iterable
        records = self.request_records(dict())

        def get_jsonschema_type(obj):
            dtype = type(obj)

            if dtype == int:
                return th.IntegerType()
            if dtype == float:
                return th.NumberType()
            if dtype == str:
                return th.StringType()
            if dtype == bool:
                return th.BooleanType()
            if dtype == list:
                if len(obj) > 0:
                    return th.ArrayType(get_jsonschema_type(obj[0]))
                else:
                    return th.ArrayType(th.IntegerType())
            if dtype == dict:
                obj_props = []
                for key in obj.keys():
                    obj_props.append(th.Property(key, get_jsonschema_type(obj[key])))

                return th.ObjectType(*obj_props)

            raise ValueError(f"Unmappable data type '{dtype}'.")

        properties = []
        property_names = set()
        # Loop through all records – some objects have different keys
        for record in records:
            # Loop through each key in the object
            for name in record.keys():
                if name in property_names:
                    continue
                # Add the new property to our list
                property_names.add(name)
                properties.append(th.Property(name, get_jsonschema_type(record[name])))
        # Return the list as a JSON Schema dictionary object
        property_list = th.PropertiesList(*properties).to_dict()
        # Cache the property list
        self._tap.cache["schema"] = property_list

        return property_list


class UsersListsStream(IterableStream):
    """Users Lists stream."""
    name = "users_lists"
    path = "/users/getByEmail"
    records_jsonpath = "$.user[*]"
    primary_keys = ["email"]
    parent_stream_type = ListsSelectStream

    schema = th.PropertiesList(
        th.Property("userId", th.StringType),
        th.Property("email", th.StringType),
        th.Property("dataFields", th.CustomType({"type": ["object", "strings"]})),
        th.Property("lists", th.ArrayType(th.IntegerType))
    ).to_dict()

    def request_records(self, context):
        decorated_request = self.request_decorator(self._request)

        user_lists = []
        for list in context.get("selected_lists", []):
            context = dict(list=list)
            prepared_request = self.custom_request("/lists/getUsers", dict(listId=list))
            resp = decorated_request(prepared_request, context)
            if resp.text:
                user_list = [{list: u} for u in resp.text.strip().split("\n")]
                user_lists.extend(user_list)
        users = set([next(iter(u.values())) for u in user_lists])
        for user in users:
            context = dict(user=user)
            prepared_request = self.custom_request(self.path, dict(email=user))
            resp = decorated_request(prepared_request, context)
            for row in self.parse_response(resp):
                row["lists"] = [next(iter(l)) for l in user_lists if next(iter(l.values()))==user]
                yield row

    def parse_response(self, response: requests.Response) -> Iterable[dict]:
        yield from extract_jsonpath(self.records_jsonpath, input=response.json())