# tap-iterable

This is a [Singer](https://singer.io) tap that produces JSON-formatted data
following the [Singer spec](https://github.com/singer-io/getting-started/blob/master/SPEC.md).

This tap:

- Pulls raw data from [Iterable](https://iterable.com/developers/)
- Extracts the following resources:
  + lists
  + users
- Outputs the schema for each resource

## Quick Start

1. Install

2. Create the config file

   Create a JSON file called `config.json`. Its contents should look like:

```json
{
  "api_key": "xx",
  "start_date" : "2018-02-22T02:06:58.147Z"
}
```

3. Run the Tap in Discovery Mode

    tap-iterable --config config.json --discover > catalog.json

   See the Singer docs on discovery mode
   [here](https://github.com/singer-io/getting-started/blob/master/docs/DISCOVERY_MODE.md#discovery-mode).

5. Run the Tap in Sync Mode

    tap-iterable --config config.json --catalog catalog-selected.json


